import flask
import googlemaps
import bluetooth
import struct

gmaps = googlemaps.Client(key = 'AIzaSyCK16wTwgKVNO6cYhURe59ruqrmrWa68aA')

app = flask.Flask("__main__")

@app.route('/')

def my_index():
    return flask.render_template('index.html', token = 'hello world')

@app.route('/result', methods = ['GET'])
def input_data():
    location = flask.request.args.get('location', None)
    
    driver_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    nearby_devices = bluetooth.discover_devices(lookup_names = True)

    for name, addr in nearby_devices:
        if("HC" in addr):
            for i in range(14):
                try:
                    driver_socket.connect((name,i))
                except OSError:
                    continue
            
            
    try:
        address = location
        coor = gmaps.geocode(address)
        lat = coor[0]["geometry"]["location"]['lat']
        lng = coor[0]["geometry"]["location"]['lng']
        data = str(lat) + ":" + str(lng)
        driver_socket.send(data)
        driver_socket.close()
    except IndexError:
        driver_socket.close()
        return "Address not available" 
    except OSError:
        return "Device is not connected, try again"
    return {'lat':lat, 'lng':lng}

@app.route('/result2', methods = ['GET'])
def example():
    #location = flask.request.args.get('location', None)
    
    driver_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    nearby_devices = bluetooth.discover_devices(lookup_names = True)

    for name, addr in nearby_devices:
        if("HC" in addr):
            for i in range(14):
                try:
                    driver_socket.connect((name,i))
                except OSError:
                    continue
            
            
    try:
        #address = location
        #coor = gmaps.geocode(address)
        #lat = coor[0]["geometry"]["location"]['lat']
        #lng = coor[0]["geometry"]["location"]['lng']
        data = str(37.3351874) + ":" + str(-121.8810715)
        driver_socket.send(data)
        driver_socket.close()
    except IndexError:
        driver_socket.close()
        return "Address not available" 
    except OSError:
        return "Device is not connected, try again"
    return {'lat':37.3351874, 'lng':-121.8810715}


app.run(debug = True)