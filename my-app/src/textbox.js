import React from 'react'

export default class Textbox extends React.Component{
    constructor(props){
        super(props);
        this.state = {value: '',
                     lat: '',
                     long: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value :event.target.value})
    }

    handleSubmit(event) {
        console.log("making request")
        fetch('/result', {
            method: "POST",
            cache: "no-cache",
            headers:{
                "content_type":"application/json",
            },
            body:JSON.stringify(this.state.value)
        })
          .then(response => {
            console.log(response)
            return response.json()
          })
          .then(json => {
          console.log=(json)
          this.setState({lat: json[0], lng: json[1]})
          })
      }
    
    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit} method = "get" action="http://localhost:5000/result">
                    <label>
                        Location:
                        <input type = "text" name = "location"/>
                        <input text = "submit" type = "submit" onChange={this.handleChange} value = {this.state.value} />
                    </label>
                        <p>
                            {this.state.lat}
                        </p>
                </form>
            </div>
            
        );
    }
}